#!/bin/sh

set -e

rm -f /tmp/pwn

cd debian/tests/files

php -S 127.0.0.1:8080 2>/dev/null &

cat <<EOF | expect
spawn phpsploit -t http://127.0.0.1:8080/vuln.php
send "exploit\n"
expect
send "run cp /etc/passwd /tmp/pwn\n"
expect
send "exit\n"
expect
send "exit --force\n"
EOF

pkill php


echo
ls -l /tmp/pwn
test -f /tmp/pwn
